# Drupal Content Extractor

Extracts current content of drupal website to be compatible with YAML extractor

## Usage
Go to your drupal custom module folder, then run 
`git clone git@gitlab.com:erwindaez/drupal-content-extractor.git`

## Reference of format
[YAML Content documentation](https://www.drupal.org/docs/8/modules/yaml-content/examples-and-snippets-v8x-1x)