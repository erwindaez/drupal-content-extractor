<?php

namespace Drupal\content_extractor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\content_extractor\Annotation\FieldExtractor;

/**
 * Manages discovery and instantiation of YAML Content process plugins.
 */
class FieldExtractorManager extends DefaultPluginManager {

  /**
   * Constructs a YamlContentProcessManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct("Plugin/field_extractors", $namespaces, $module_handler,
      FieldExtractorInterface::class,
      FieldExtractor::class);
    $this->setCacheBackend($cache_backend, 'field_extractors_plugins');
  }

}
