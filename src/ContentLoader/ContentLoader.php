<?php

namespace Drupal\content_extractor\ContentLoader;

use Drupal\yaml_content\Plugin\ProcessingContext;
use Drupal\yaml_content\ContentLoader\ContentLoader as YAMLContentLoader;

/**
 * ContentLoader class for parsing and importing YAML content.
 */
class ContentLoader extends YAMLContentLoader {

  /**
   * Populate field content into the provided field.
   *
   * @param object $field
   *   The entity field object.
   * @param array $field_data
   *   The field data.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @todo Handle field data types more dynamically with typed data.
   */
  public function populateField($field, array &$field_data) {
    // Get the field cardinality to determine whether or not a value should be
    // 'set' or 'appended' to.
    $cardinality = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();

    // Gets the count of the field data array.
    $field_data_count = count($field_data);

    // If the cardinality is 0, throw an exception.
    if (!$cardinality) {
      throw new \InvalidArgumentException("'{$field->getName()}' cannot hold any values.");
    }

    // If the number of field content is greater than allowed, throw exception.
    if ($cardinality > 0 && $field_data_count > $cardinality) {
      throw new \InvalidArgumentException("'{$field->getname()}' cannot hold more than $cardinality values. $field_data_count values were parsed from the YAML file.");
    }

    // If we're updating content in-place, empty the field before population.
    if ($this->existenceCheck() && !$field->isEmpty()) {
      // Trigger delete callbacks on each field item.
      $field->delete();

      // Empty out the field's list of items.
      $field->setValue([]);
    }

    // Iterate over each field data value and process it.
    foreach ($field_data as &$item_data) {
      if (isset($field_data['#process']['dependency'])) {
        $dependency = $field_data['#process']['dependency'];
        $this->processDependency($dependency);
      }
      // Preprocess the field data.
      $context = new ProcessingContext();
      $context->setField($field);
      $context->setContentLoader($this);
      $this->getProcessManager()->preprocessFieldData($context, $item_data);

      // Check if the field is a reference field. If so, build the entity ref.
      $is_reference = isset($item_data['entity']);
      if ($is_reference) {
        // Build the reference entity.
        $field_item = $this->buildEntity($item_data['entity'], $item_data);
      }
      else {
        $field_item = $item_data;
      }

      // If the cardinality is set to 1, set the field value directly.
      if ($cardinality == 1) {
        $field->setValue($field_item);

        // @todo Warn if additional item data is available for population.
        break;
      }
      else {
        // Otherwise, append the item to the multi-value field.
        $field->appendItem($field_item);
      }
    }
  }

  /**
   * Create the entity based on basic properties.
   *
   * If existence checking is enabled, we'll attempt to load an existing
   * entity matched on the simple properties before creating a new one.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array $content_data
   *   The array of content data to be parsed.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A loaded matching entity if existence checking is enabled and a matching
   *   entity was found, or a new one stubbed from simple properties otherwise.
   *
   * @see \Drupal\yaml_content\ContentLoader\ContentLoader::existenceCheck()
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function createEntity($entity_type, array $content_data) {
    // If existence checking is enabled, attempt to load the entity first.
    if ($this->existenceCheck()) {
      if ($content_data['uuid']) {
        $entity = $this->entityExists($entity_type, ['uuid' => $content_data['uuid']]);
      }
      else {
        $entity = $this->entityExists($entity_type, $content_data);
      }
    }
    $attributes = $this->getContentAttributes($entity_type, $content_data);

    // If the entity isn't loaded we'll stub it out.
    if (empty($entity)) {
      // Load entity type handler.
      $entity_handler = $this->getEntityStorage($entity_type);

      // Identify the content properties for the entity.
      $attributes = $this->getContentAttributes($entity_type, $content_data);

      $entity = $entity_handler->create($attributes['property']);
    }

    return $entity;
  }

  /**
   * Query if a target entity already exists and should be updated.
   *
   * @param string $entity_type
   *   The type of entity being imported.
   * @param array $content_data
   *   The import content structure representing the entity being searched for.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false
   *   Return a matching entity if one is found, or FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *
   * @todo Potentially move this into a separate helper class.
   */
  public function entityExists($entity_type, array $content_data) {

    // Load entity type handler.
    $entity_handler = $this->getEntityStorage($entity_type);

    $query = $entity_handler->getQuery();
    foreach ($content_data as $key => $value) {
      if ($key != 'entity' && !is_array($value)) {
        $query->condition($key, $value);
      }
    }
    $entity_ids = $query->execute();

    if ($entity_ids) {
      $entity_id = array_shift($entity_ids);
      $entity = $entity_handler->load($entity_id);
    }
    return isset($entity) ? $entity : FALSE;
  }

}
