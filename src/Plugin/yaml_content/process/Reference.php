<?php

namespace Drupal\content_extractor\Plugin\yaml_content\process;

use Drupal\yaml_content\Plugin\ProcessingContext;
use Drupal\yaml_content\Plugin\yaml_content\process\Reference as YAMLReference;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @YamlContentProcess(
 *   id = "ce_reference",
 *   title = @Translation("Entity Reference Processor"),
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class Reference extends YAMLReference {

  /**
   * {@inheritdoc}
   */
  public function process(ProcessingContext $context, array &$field_data) {
    $entity_type = $this->configuration[0];
    $filter_params = $this->configuration[1];

    $entity_storage = $this->entityTypeManager->getStorage($entity_type);

    // Use query factory to create a query object for the node of entity_type.
    $query = $entity_storage->getQuery('AND');

    // Apply filter parameters.
    foreach ($filter_params as $property => $value) {
      $query->condition($property, $value);
    }

    $entity_ids = $query->execute();

    if (empty($entity_ids)) {
      $entity = $entity_storage->create($filter_params);
      $entity_ids = [$entity->id()];
    }

    if (!empty($entity_ids)) {
      // Use the first match for our value.
      $field_data['target_id'] = array_shift($entity_ids);

      // Remove process data to avoid issues when setting the value.
      unset($field_data['#process']);

      if ($entity_type === 'paragraph') {
        $paragraphs_entity = $entity_storage->load($field_data['target_id']);
        $field_data = [
          'target_id' => $paragraphs_entity->id(),
          'target_revision_id' => $paragraphs_entity->getRevisionId(),
        ];
      }
      return $entity_ids;
    }
    $this->throwParamError('Unable to find referenced content', $entity_type, $filter_params);
  }

}
