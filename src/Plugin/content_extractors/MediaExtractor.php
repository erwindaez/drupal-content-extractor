<?php

namespace Drupal\content_extractor\Plugin\content_extractors;

use Drupal\content_extractor\ContentExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @ContentExtractor(
 *   id = "media",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class MediaExtractor extends ContentExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getType() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getBundle() {
    return $this->entity->bundle();
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle() {
    return $this->entity->get('name')->getValue()[0]['value'];
  }

  /**
   * {@inheritDoc}
   */
  public function getBasicData() {
    return [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'name' => $this->getTitle(),
      'status' => $this->getStatus(),
      'bundle' => $this->getBundle(),
    ];
  }

}
