<?php

namespace Drupal\content_extractor\Plugin\content_extractors;

use Drupal\content_extractor\ContentExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @ContentExtractor(
 *   id = "taxonomy_term",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class TaxonomyTermExtrator extends ContentExtractorBase {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->entity->label();
  }

  /**
   * {@inheritDoc}
   */
  public function getType($entity = NULL) {
    if (empty($entity)) {
      return $this->entity->get('vid')->getValue()[0]['target_id'];
    }
    return $entity->get('vid')->getValue()[0]['target_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getParents() {
    $processed_parent = [];
    $parents = $this->entityTypeManager->getStorage($this->entityType)->loadParents($this->entity->id());
    foreach ($parents as $parent) {
      $processed_parent[] = [
        'entity' => $parent->getEntityTypeId(),
        'uuid' => $this->getUuid($parent),
      ];
    }
    return $processed_parent;
  }

  /**
   * Get weight.
   *
   * @return int
   *   Gets Weight.
   */
  public function getWeight() {
    return $this->entity->getWeight();
  }

  /**
   * Get weight.
   *
   * @return int
   *   Gets Weight.
   */
  public function getStatus() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function getBasicData() {
    return [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'vid' => $this->getType(),
      'tid' => $this->getId(),
      'status' => $this->getStatus(),
      'name' => $this->getTitle(),
      'weight' => $this->getWeight(),
      'path' => $this->getPath(),
    ];
  }

}
