<?php

namespace Drupal\content_extractor\Plugin\content_extractors;

use Drupal\content_extractor\ContentExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @ContentExtractor(
 *   id = "menu_link_content",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class MenuExtractor extends ContentExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getTitle() {
    return $this->entity->getTitle();
  }

  /**
   * Get Menu Name.
   *
   * @return string
   *   Gets Menu Name. Applicable for MenuExtractor Class only.
   */
  public function getMenuName() {
    return $this->entity->getMenuName();
  }

  /**
   * Get Menu weight.
   *
   * @return int
   *   Gets Menu Weight. Applicable for MenuExtractor Class only.
   */
  public function getWeight() {
    return $this->entity->getWeight();
  }

  /**
   * Check if enabled.
   *
   * @return bool
   *   Check if menu item is enabled.
   */
  public function isEnabled() {
    return $this->entity->isEnabled();
  }

  /**
   * Get link of menu.
   *
   * @return string
   *   Gets link of the menu.
   */
  public function getLink() {
    return $this->entity->get('link')->uri;
  }

  /**
   * {@inheritDoc}
   */
  public function getParents() {
    $parent_id = $this->entity->getParentId();
    $processed_parent = [];
    if (!empty($parent_id)) {
      $uuid = explode(':', $parent_id)[1];
      $parents = $this->entityTypeManager->getStorage($this->entityType)->loadByProperties(['uuid' => $uuid]);
      foreach ($parents as $parent) {
        $processed_parent[] = [
          'entity' => $this->entityType,
          'uuid' => $this->getUuid($parent),
        ];
      }
    }
    return $processed_parent;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityData() {
    $data = [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'title' => $this->getTitle(),
      'menu_name' => $this->getMenuName(),
      'weight' => $this->getWeight(),
      'enabled' => $this->isEnabled(),
      'link' => $this->getLink(),
      'parent' => $this->getParents(),
    ];
    return $data;
  }

}
