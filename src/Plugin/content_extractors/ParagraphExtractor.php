<?php

namespace Drupal\content_extractor\Plugin\content_extractors;

use Drupal\content_extractor\ContentExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @ContentExtractor(
 *   id = "paragraph",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class ParagraphExtractor extends ContentExtractorBase {

  /**
   * {@inheritdoc}
   */
  public function getBundle() {
    return $this->entity->bundle();
  }

  /**
   * {@inheritDoc}
   */
  public function getBasicData() {
    return [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'type' => $this->getBundle(),
      'title' => $this->getTitle(),
      'status' => $this->getStatus(),
    ];
  }

}
