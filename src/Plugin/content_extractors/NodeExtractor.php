<?php

namespace Drupal\content_extractor\Plugin\content_extractors;

use Drupal\content_extractor\ContentExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @ContentExtractor(
 *   id = "node",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class NodeExtractor extends ContentExtractorBase {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->entity->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle() {
    return $this->entity->bundle();
  }

  /**
   * {@inheritDoc}
   */
  public function getBasicData() {
    return [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'nid' => $this->getId(),
      'type' => $this->getBundle(),
      'title' => $this->getTitle(),
      'status' => $this->getStatus(),
      'path' => $this->getPath(),
    ];
  }

}
