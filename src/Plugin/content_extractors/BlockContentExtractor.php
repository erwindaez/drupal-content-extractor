<?php

namespace Drupal\content_extractor\Plugin\content_extractors;

use Drupal\content_extractor\ContentExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @ContentExtractor(
 *   id = "block_content",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class BlockContentExtractor extends ContentExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return $this->entity->label();
  }

  /**
   * {@inheritDoc}
   */
  public function getBasicData() {
    return [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'type' => $this->getType(),
      'info' => $this->getInfo(),
      'status' => $this->getStatus(),
    ];
  }

}
