<?php

namespace Drupal\content_extractor\Plugin\field_extractors;

use Drupal\content_extractor\FieldExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @FieldExtractor(
 *   id = "default",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class DefaultFieldExtractor extends FieldExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return $this->entity->label();
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldValue() {
    return $this->field->value;
  }

}
