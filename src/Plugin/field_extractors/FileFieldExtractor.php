<?php

namespace Drupal\content_extractor\Plugin\field_extractors;

use Drupal\content_extractor\FieldExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @FieldExtractor(
 *   id = "file",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class FileFieldExtractor extends FieldExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getFieldValue() {
    if (empty($this->field->entity)) {
      return NULL;
    }
    $uri = $this->field->entity->getFileUri();
    $fileName = $this->field->entity->getFilename();
    $this->extractor->moveFile($uri, $fileName);

    // Process args.
    $args = [
      'image',
      [
        'type' => 'module',
        'filename' => $fileName,
      ],
    ];
    return [
      [
        '#process' => [
          'callback' => 'file',
          'args' => $args,
        ],
        'alt' => $fileName,
      ],
    ];

  }

}
