<?php

namespace Drupal\content_extractor\Plugin\field_extractors;

use Drupal\content_extractor\FieldExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @FieldExtractor(
 *   id = "plugin_reference",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class PluginFieldExtractor extends FieldExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return $this->entity->label();
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldValue() {
    $values = $this->field->getValue();
    return $values;
  }

}
