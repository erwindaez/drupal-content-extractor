<?php

namespace Drupal\content_extractor\Plugin\field_extractors;

use Drupal\content_extractor\FieldExtractorBase;

/**
 * Plugin for querying and loading a referenced entity.
 *
 * @FieldExtractor(
 *   id = "reference",
 *   description = @Translation("Attach an entity reference.")
 * )
 */
class ReferenceFieldExtractor extends FieldExtractorBase {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return $this->entity->label();
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldValue() {
    $value_entities = $this->field->referencedEntities();
    $values = [];
    foreach ($value_entities as $entity) {
      // Process args.
      $uuid = ['uuid' => $entity->hasField('uuid') && !$entity->get('uuid')->isEmpty() ? $entity->get('uuid')->value : ''];
      $args = [
        $entity->getEntityTypeId(),
        $uuid,
      ];

      $values[] = [
        '#process' => [
          'callback' => 'ce_reference',
          'args' => $args,
        ],
      ];
    }

    return $values;
  }

}
