<?php

namespace Drupal\content_extractor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\content_extractor\Annotation\ContentExtractor;

/**
 * Manages discovery and instantiation of YAML Content process plugins.
 */
class ContentExtractorManager extends DefaultPluginManager {

  /**
   * Constructs a YamlContentProcessManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct("Plugin/content_extractors", $namespaces, $module_handler,
      ContentExtractorInterface::class,
      ContentExtractor::class);

    $this->setCacheBackend($cache_backend, 'content_extractors_plugins');
  }

  /**
   * Get Data of array.
   *
   * @param string $entityType
   *   Single drupal entity type.
   *
   * @return array
   *   Returns the formed data of single drupal entity.
   *   The formed data would be used for YML construction.
   */
  public function getData($entityType) {

    $storage = \Drupal::service('entity_type.manager')->getStorage($entityType);
    $entities = $storage->loadMultiple();
    $data = [];
    foreach ($entities as $entity) {
      $plugin = $this->getContentEntity($entityType, ['entity_type' => $entityType, 'entity' => $entity]);
      $data[] = $plugin->getEntityData();
    }

    return $data;
  }

  /**
   * Returns Content Entity for use.
   *
   * @return Drupal\content_extractor\ContentEntityInterface
   *   Content Entity Interface.
   */
  private function getContentEntity($entityType, $entity) {
    $plugin = $this->createInstance($entityType, $entity);
    return $plugin;
  }

}
