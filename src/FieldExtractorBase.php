<?php

namespace Drupal\content_extractor;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritDoc}
 */
abstract class FieldExtractorBase implements FieldExtractorInterface, ContainerFactoryPluginInterface {

  /**
   * Entity Type provided.
   *
   * @var string
   */
  protected $fieldType;

  /**
   * Entity provided.
   *
   * @var mixed
   */
  protected $field;


  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Extractor Helper.
   *
   * @var \Drupal\content_extractor\ExtractorHelper
   */
  protected $extractor;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->fieldType = $configuration['field_type'];
    $instance->field = $configuration['field'];
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->extractor = $container->get('content_extractor.extractor');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getUuid($entity = NULL) {
    if (is_null($entity)) {
      return $this->entity->get('uuid')->value;
    }
    return $entity->get('uuid')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldValue() {
    return [];
  }

}
