<?php

namespace Drupal\content_extractor\Service;

use Drupal\yaml_content\Service\EntityLoadHelper as YAMLEntityLoadHelper;

/**
 * A helper class to support identification and loading of existing entities.
 */
class EntityLoadHelper extends YAMLEntityLoadHelper {

  /**
   * An array of entity type machine names that require special handling.
   *
   * The entity types listed in this array cannot be loaded and treated the same
   * as other entity types and require special attention.
   *
   * @var string[]
   *   requiresSpecialHandling
   *
   * @see https://www.drupal.org/project/yaml_content/issues/2893055
   */
  protected static $requiresSpecialHandling = [
    'media',
    'file',
  ];

}
