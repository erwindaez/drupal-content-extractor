<?php

namespace Drupal\content_extractor\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class ContentExtractor.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class FieldExtractor extends Plugin {
  /**
   * A brief, human readable, description of the sandwich type.
   *
   * This property is designated as being translatable because it will appear
   * in the user interface. This provides a hint to other developers that they
   * should use the Translation() construct in their annotation when declaring
   * this property.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
