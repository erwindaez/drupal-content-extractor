<?php

namespace Drupal\content_extractor;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritDoc}
 */
abstract class ContentExtractorBase implements ContentExtractorInterface, ContainerFactoryPluginInterface {

  /**
   * Entity Type provided.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Entity provided.
   *
   * @var mixed
   */
  protected $entity;

  /**
   * Field Extractor.
   *
   * @var \Drupal\content_extractor\FieldExtractorManager
   */
  protected $fieldExtractor;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Extractor Helper.
   *
   * @var \Drupal\content_extractor\ExtractorHelper
   */
  protected $extractor;

  /**
   * Extractor Helper.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $pathAliasManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->fieldExtractor = $container->get('content_extractor.field_extractor_manager');
    $instance->entity = $configuration['entity'];
    $instance->entityType = $configuration['entity_type'];
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->extractor = $container->get('content_extractor.extractor');
    $instance->pathAliasManager = $container->get('path.alias_manager');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFields() {
    $fields = $this->entity->getFields(FALSE);
    $processed_fields = [];

    foreach ($fields as $key => $field) {
      if (strpos($key, 'field_') === 0) {
        $field_type = $field->getFieldDefinition()->getType();
        $field_map = [
          'entity_reference_revisions' => 'reference',
          'entity_reference' => 'reference',
          'address' => 'plugin_reference',
          'link' => 'plugin_reference',
          'block_field' => 'plugin_reference',
          'image' => 'file',
        ];

        $plugin_id = 'default';

        if (array_key_exists($field_type, $field_map)) {
          $plugin_id = $field_map[$field_type];
        }

        $configuration = [
          'field_type' => $field_type,
          'field' => $field,
        ];

        $plugin = $this->fieldExtractor->createInstance($plugin_id, $configuration);
        $processed_fields[$key] = $plugin->getFieldValue();
      }
    }
    return $processed_fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getBundle() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getParents() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle() {
    return NULL;
  }

  /**
   * Get Menu weight.
   *
   * @return int
   *   Gets Entity Weight.
   */
  public function getWeight() {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getPath($entity = NULL) {
    if (empty($entity)) {
      $entity = $this->entity;
    }

    if ($entity->hasField('path') && !empty($entity->get('path')->getValue()[0]['alias'])) {
      return [
        [
          'alias' => $entity->get('path')->getValue()[0]['alias'],
          // Unable to determine if pathauto is active.
          // Therefore always set as FALSE.
          'pathauto' => FALSE,
        ],
      ];
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getUuid($entity = NULL) {
    if (is_null($entity)) {
      return $this->entity->get('uuid')->value;
    }
    return $entity->get('uuid')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getType() {
    return $this->entity->get('type')->getValue()[0]['target_id'];
  }

  /**
   * {@inheritDoc}
   */
  public function getStatus() {
    // TODO: Do some logic to get status of block content.
    return 1;
  }

  /**
   * {@inheritDoc}
   */
  public function getBasicData() {
    return [
      'entity' => $this->entityType,
      'uuid' => $this->getUuid(),
      'type' => $this->getType(),
      'info' => $this->getInfo(),
      'title' => $this->getTitle(),
      'status' => $this->getStatus(),
      'bundle' => $this->getBundle(),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityData() {
    $data = $this->getBasicData();

    $fields_data = $this->getFields();
    if (!empty($fields_data)) {
      $data = array_merge($data, $fields_data);
    }

    $parents = $this->getParents();
    if (!empty($parents)) {
      $data = array_merge($data, ['parents' => $parents]);
    }

    return array_filter($data);
  }

  /**
   * Get ID.
   */
  public function getId() {
    return $this->entity->id();
  }

}
