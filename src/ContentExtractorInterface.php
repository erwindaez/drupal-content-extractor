<?php

namespace Drupal\content_extractor;

/**
 * Defines an interface for the content entity to be extracted.
 */
interface ContentExtractorInterface {

  /**
   * Get UUID of single entity.
   *
   * @return string
   *   Returns the Uuid of entity.
   */
  public function getUuid();

  /**
   * Get Path alias of single entity.
   *
   * @return string
   *   Returns the Path of entity.
   */
  public function getPath();

  /**
   * Get info/ title of single entity.
   *
   * @return null|string
   *   Returns title of single entity.
   *   Returns null if info is not available or needed.
   */
  public function getInfo();

  /**
   * Get ID.
   *
   * @return null|string
   *   Returns ID.
   *   Returns null if info is not available or needed.
   */
  public function getId();

  /**
   * Get type of single entity, returns NULL if not possible.
   *
   * @return null|string
   *   Returns type of entity. Mostly it is the machine name.
   */
  public function getType();

  /**
   * Get status of single entity/ 1 if published or active 0 if not.
   *
   * @return bool
   *   Returns true if active/published.
   */
  public function getStatus();

  /**
   * Get fields of the selected entity.
   *
   * @return array
   *   Returns formatted array of fields that would be used for YML creation.
   */
  public function getFields();

  /**
   * Get bundle of the selected entity.
   *
   * @return array|null
   *   Returns bundle type if it exists.
   *   Return null if not possible.
   */
  public function getBundle();

  /**
   * Get bundle of the selected entity.
   *
   * @return array|null
   *   Returns bundle type if it exists.
   *   Return null if not possible.
   */
  public function getTitle();

  /**
   * Get bundle of the selected entity.
   *
   * @return array|null
   *   Returns bundle type if it exists.
   *   Return null if not possible.
   */
  public function getParents();

  /**
   * Get bundle of the selected entity.
   *
   * @return array|null
   *   Returns bundle type if it exists.
   *   Return null if not possible.
   */
  public function getEntityData();

  /**
   * Get bundle of the selected entity.
   *
   * @return array|null
   *   Returns bundle type if it exists.
   *   Return null if not possible.
   */
  public function getBasicData();

  /**
   * Get Weight of the selected entity.
   *
   * @return int|null
   *   Returns bundle type if it exists.
   *   Return null if not possible.
   */
  public function getWeight();

}
