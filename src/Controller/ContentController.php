<?php

namespace Drupal\content_extractor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\content_extractor\ExtractorHelper;

/**
 * {@inheritdoc}
 */
class ContentController extends ControllerBase {

  /**
   * The helper class.
   *
   * @var \Drupal\content_extractor\ExtractorHelper
   *  The helper class
   */
  protected $helper;

  /**
   * Constructs the object.
   *
   * @param \Drupal\content_extractor\ExtractorHelper $helper
   *   The helper class.
   */
  public function __construct(ExtractorHelper $helper) {
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_extractor.extractor')
    );
  }

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    $this->helper->getContent('taxonomy_term');
    $build = [
      '#markup' => "",
    ];
    return $build;
  }

  /**
   * Returns a render-able array for a test page.
   */
  public function test() {
    \Drupal::service('yaml_content.load_helper')->importModule('content_extractor');

    $build = [
      '#markup' => "",
    ];
    return $build;
  }

}
