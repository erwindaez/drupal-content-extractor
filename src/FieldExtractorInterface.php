<?php

namespace Drupal\content_extractor;

/**
 * Defines an interface for the content entity to be extracted.
 */
interface FieldExtractorInterface {

  /**
   * Get UUID of single entity.
   *
   * @return string
   *   Returns the Uuid of entity.
   */
  public function getUuid();

  /**
   * Get info/ title of single entity.
   *
   * @return null|string
   *   Returns title of single entity.
   *   Returns null if info is not available or needed.
   */
  public function getFieldValue();

}
