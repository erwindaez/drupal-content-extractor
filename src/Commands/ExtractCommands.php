<?php

namespace Drupal\content_extractor\Commands;

use Drush\Commands\DrushCommands;
use Drupal\content_extractor\ImportHelper;
use Drupal\content_extractor\ExtractorHelper;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class ExtractCommands extends DrushCommands {

  /**
   * Extractor Helper.
   *
   * @var \Drupal\content_extractor\ExtractorHelper
   */
  protected $extractor;

  /**
   * Extractor Helper.
   *
   * @var \Drupal\content_extractor\ImportHelper
   */
  protected $importer;

  /**
   * ContentLoader constructor.
   *
   * @param \Drupal\content_extractor\ExtractorHelper $extractor
   *   Content Extractor extractor service.
   * @param \Drupal\content_extractor\ImportHelper $importer
   *   Content Extractor import service.
   */
  public function __construct(ExtractorHelper $extractor, ImportHelper $importer) {
    $this->extractor = $extractor;
    $this->importer = $importer;
  }

  /**
   * Extract current content from your site to yml file.
   *
   * @param string $module
   *   The machine name of a module to be searched for content.
   * @param string $entity_type
   *   The machine name of a module to be searched for content.
   *
   * @command content_extractor:generate
   * @aliases ce-gen
   * @usage content_extractor:generate
   *   Extract content from you current site
   */
  public function generate($module, $entity_type) {
    $this->extractor->setModule($module);
    $module_path = drupal_get_path('module', $this->extractor->getModule());

    $allowed_types = [
      'media',
      'paragraph',
      'block_content',
      'taxonomy_term',
      'node',
      'menu_link_content',
    ];

    if (!in_array($entity_type, $allowed_types)) {
      return 'Entity type is not allowed to be exported';
    }

    $content = "";

    $content .= $this->extractor->getContent($entity_type);

    $destination = $module_path . '/content/';

    // Create the destination directory if it does not already exist.
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

    $file_name = $entity_type . '.content.yml';
    // Save the file data or return an existing file.
    $this->extractor->writeToFile($destination . '/' . $file_name, $content);

    $this->output()->writeln('Content extracted');
  }

  /**
   * Extract current content from your site to yml file.
   *
   * @param string $module
   *   The machine name of a module to be searched for content.
   *
   * @command content_extractor:generate-all
   * @aliases ce-gen-all
   * @usage content_extractor:generate
   *   Extract content from you current site
   */
  public function generateAll($module) {
    $this->extractor->setModule($module);
    $module_path = drupal_get_path('module', $this->extractor->getModule());

    $destination = $module_path . '/content/';
    // Create the destination directory if it does not already exist.
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

    $path = $destination . '/demo.content.yml';
    $content = "";

    // TODO: Generate Media.
    $content .= $this->extractor->getContent('media');
    // TODO: Generate Taxonomy.
    $content .= $this->extractor->getContent('taxonomy_term');
    // TODO: Generate Paragraph.
    $content .= $this->extractor->getContent('paragraph');
    // Generate Block.
    $content .= $this->extractor->getContent('block_content');
    // TODO: Generate Node.
    $content .= $this->extractor->getContent('node');
    // TODO: Generate Menu.
    $content .= $this->extractor->getContent('menu_link_content');
    $this->extractor->writeToFile($path, $content);
    $this->output()->writeln('Content extracted');
  }

  /**
   * Import current content from yml file to your site.
   *
   * @param string $module
   *   The machine name of a module to be searched for content.
   *
   * @command content_extractor:import
   * @aliases ce-im
   * @usage content_extractor:import
   *   Import content from you current site
   */
  public function import($module) {
    $this->importer->importModule($module);
  }

}
