<?php

namespace Drupal\content_extractor;

use Drupal\yaml_content\Service\LoadHelper;

/**
 * A helper class to support the content loading process.
 */
class ImportHelper extends LoadHelper {

  /**
   * Scan and discover content files for import.
   *
   * The scanner assumes all content files will follow the naming convention of
   * '*.content.yml'.
   *
   * @param string $path
   *   The directory path to be scanned for content files.
   * @param string $mask
   *   (Optional) A file name mask to limit matches in scanned files.
   *
   * @return array
   *   An associative array of objects keyed by filename with the following
   *   properties as returned by file_scan_directory():
   *
   *   - 'uri'
   *   - 'filename'
   *   - 'name'
   *
   * @see file_scan_directory()
   */
  public function discoverFiles($path, $mask = '/.*\.content\.yml/') {
    // Identify files for import.
    $files = file_scan_directory($path, $mask, [
      'key' => 'filename',
      'recurse' => FALSE,
    ]);

    // Sort the files to ensure consistent sequence during imports.
    ksort($files);

    // Sort the files based on entity types.
    $order = [
      'media.content.yml',
      'taxonomy_term.content.yml',
      'paragraph.content.yml',
      'block_content.content.yml',
      'node.content.yml',
      'menu_link_content.content.yml',
    ];

    $files = array_merge(array_flip($order), $files);

    return $files;
  }

}
