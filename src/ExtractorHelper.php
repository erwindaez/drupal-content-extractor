<?php

namespace Drupal\content_extractor;

use Drupal\Component\Serialization\Yaml;

/**
 * Helper class for content extractor.
 */
class ExtractorHelper {


  /**
   * Undocumented variable.
   *
   * @var [type]
   */
  protected $contentManager;

  /**
   * YAML parser.
   *
   * @var \Symfony\Component\Yaml\Yaml
   */
  protected $yaml;

  /**
   * Module name.
   *
   * @var string
   */
  protected $module;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContentExtractorManager $contentManager) {
    $this->contentManager = $contentManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getContent($entityType) {
    $data = $this->contentManager->getData($entityType);

    $data = preg_replace('/-\n */', '- ', $this->getYaml()->encode($data));
    $data = str_replace(' null', ' { }', $data);
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setModule($module) {
    $this->module = $module;
  }

  /**
   * {@inheritdoc}
   */
  public function getModule() {
    return $this->module;
  }

  /**
   * Get the YAML parser service.
   *
   * @return \Symfony\Component\Yaml\Parser
   *   The YAML parser service.
   */
  protected function getYaml() {
    if (!isset($this->yaml)) {
      $this->yaml = new Yaml();
    }
    return $this->yaml;
  }

  /**
   * Moves file on module folder.
   *
   * @param string $uri
   *   Uri Path of file.
   * @param string $fileName
   *   File name.
   *
   * @return bool
   *   Return success status.
   */
  public function moveFile(string $uri, string $fileName) {
    $module_path = drupal_get_path('module', $this->getModule());

    $destination = $module_path . '/images/';
    // Create the destination directory if it does not already exist.
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

    return copy($uri, $destination . '/' . $fileName);
  }

  /**
   * Write and save file to your YML.
   */
  public function writeToFile($path, $content) {
    $file = fopen($path, 'w');
    fwrite($file, $content);
    fclose($file);
  }

}
